instancetype=t1.micro                # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-types.html#AvailableInstanceTypes
hostname=Learning
keyname=DemoKeyPair
imageid=ami-06705195ce845509c          # Ubuntu 18
subnetid=subnet-a99652e0
securitygroupids=sg-038f8f971fa0082e0
instanceinitiatedshutdownbehavior=terminate
iaminstanceprofile="EC2_S3_LimitedTo_volkerrath_bucket"       # aws iam list-instance-profiles

aws ec2 run-instances \
--image-id ${imageid} \
--count 1 \
--instance-type ${instancetype} \
--key-name ${keyname} \
--security-group-ids ${securitygroupids} \
--subnet-id ${subnetid} \
--instance-initiated-shutdown-behavior ${instanceinitiatedshutdownbehavior} \
--associate-public-ip-address \
--iam-instance-profile Name="${iaminstanceprofile}" \
--block-device-mappings 'DeviceName=/dev/sda1,Ebs={VolumeSize=8,DeleteOnTermination=true}'
--tag-specifications 'ResourceType=instance,Tags=[{Key=Role,Value=Demo}]'
#--dry-run