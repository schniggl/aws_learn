# Run command in instance using AWS Systems Manager
# AWS Policy AmazonEC2RoleforSSM  in instance role required

# Test. Create simple file in root directory in specific instance
aws ssm send-command --document-name "AWS-RunShellScript" --document-version "1" \
--targets "Key=instanceids,Values=i-079f4fe2c228273f3" \
--parameters '{"workingDirectory":["/"],"executionTimeout":["3600"],"commands":["touch /xxxxxxxxTEST"]}' \
--timeout-seconds 600 --max-concurrency "50" --max-errors "0" --region ap-southeast-2

# Execute multiple commands. Target instances selected by tag
aws ssm send-command --document-name "AWS-RunShellScript" --document-version "1" \
--targets '[{"Key":"tag:Role","Values":["Demo"]}]' \
--parameters '{"workingDirectory":[""],"executionTimeout":["3600"],"commands":["echo `date` AWS Systems Manager executed >> /tmp/AwsSsm.log","sudo apt-get update","sudo apt-get --yes install python3 python3-pip awscli"]}' \
--timeout-seconds 600 --max-concurrency "50" --max-errors "0" --region ap-southeast-2


